package com.example.task.di

@Retention(AnnotationRetention.RUNTIME)
@AppScope
annotation class AppScope {
}