package com.example.task.View

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.task.ViewModel.AppViewModel
import com.example.task.model.TaskViewState
import com.example.task.model.ViewModelUtil
import com.example.task.R
import com.example.task.model.CreateTask
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.floatingactionbutton.FloatingActionButton

class TaskActivity : AppCompatActivity() {

    lateinit var appViewModel: AppViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        appViewModel = ViewModelUtil.getAppViewModel(this)

        val navController = findNavController(R.id.nav_host_fragment)
        navController.restoreState(savedInstanceState)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        NavigationUI.setupWithNavController(toolbar, navController)
        toolbar.inflateMenu(R.menu.menu_main)

        setupCreate()

        setupDestinationListener()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.create -> true
            R.id.search -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupCreate() {
        findViewById<View>(R.id.createFab).setOnClickListener {
            if (supportFragmentManager.findFragmentByTag("CREATE_FRAGMENT") == null) {
                val createFragment = CreateFragment()
                createFragment.show(supportFragmentManager, "CREATE_FRAGMENT")
            }
        }
    }

    private fun setupDestinationListener() {

        findNavController(R.id.nav_host_fragment).apply {
            removeOnDestinationChangedListener(destinationChangedListener)
            addOnDestinationChangedListener(destinationChangedListener)
        }
    }

    private val destinationChangedListener: NavController.OnDestinationChangedListener = NavController.OnDestinationChangedListener { controller, destination, arguments ->
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        when (destination.id) {
            R.id.TaskListFragment -> {
                findViewById<FloatingActionButton>(R.id.createFab).show()
                findViewById<View>(R.id.bottomAppBar).visibility = View.VISIBLE
                toolbar.menu.findItem(R.id.create).isVisible = false
                toolbar.menu.findItem(R.id.search).isVisible = true
            }
            R.id.TaskEditFragment -> {
                findViewById<FloatingActionButton>(R.id.createFab).hide()
                findViewById<View>(R.id.bottomAppBar).visibility = View.GONE
                toolbar.menu.findItem(R.id.create).isVisible = true
                toolbar.menu.findItem(R.id.search).isVisible = false
            }
        }
    }


    override fun onStop() {
        super.onStop()
        appViewModel.taskViewState().removeObservers(this)
    }

    override fun onStart() {
        super.onStart()

        appViewModel.taskViewState().observe(this, Observer {
            when(it) {
                is CreateTask -> {
                    if (supportFragmentManager.findFragmentByTag("CREATE_FRAGMENT") != null) {
                        (supportFragmentManager.findFragmentByTag("CREATE_FRAGMENT") as BottomSheetDialogFragment).dismiss()
                    }
                }
            }
        })
    }
}