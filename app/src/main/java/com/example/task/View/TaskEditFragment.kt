package com.example.task.View

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.widget.Toolbar
import androidx.navigation.fragment.findNavController
import com.example.task.Database.TaskTable
import com.example.task.R


class TaskEditFragment : BaseTaskFragment() {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        val menuToolbar = view.findViewById<Toolbar>(R.id.menuToolbar)
        menuToolbar.visibility = View.GONE

        val taskData = viewModel.getTaskData(requireArguments().getString("taskID",""))

        val title= view.findViewById<EditText>(R.id.title)
        val time= view.findViewById<EditText>(R.id.time)
        val date= view.findViewById<EditText>(R.id.date)
        val spinner= view.findViewById<Spinner>(R.id.spinner)

        title.setText(taskData.title)
        time.setText(taskData.time)
        date.setText(taskData.date)
        val list = arrayListOf("LOW","MEDIUM" , "HIGH")
        spinner.setSelection(list.indexOf(taskData.priority))

        val toolbar = requireActivity().findViewById<Toolbar>(R.id.toolbar)

        toolbar.menu.findItem(R.id.create).setOnMenuItemClickListener {
            viewModel.updateTaskData(taskData.id,title.text.toString(),date.text.toString(),spinner.selectedItem.toString(),time.text.toString())
            findNavController().navigateUp()
            true
        }

    }

    override fun handleView(visible: Boolean) {
        val toolbar = requireActivity().findViewById<Toolbar>(R.id.toolbar)
        toolbar.menu.findItem(R.id.create).isVisible =visible
    }


}