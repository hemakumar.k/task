package com.example.task.View

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.task.Database.TaskTable
import com.example.task.R

class TaskAdapter(var dataList : List<TaskTable>, val listener: Listener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() , TaskItemTouchHelper {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view =  LayoutInflater.from(parent.context).inflate(R.layout.task_holder, parent, false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as ViewHolder
        val holderData = dataList[position]
        holder.title.text = holderData.title
        holder.time.text = holderData.time
        holder.date.text = holderData.date
        holder.priority.text = holderData.priority

        if(holderData.priority == "HIGH"){
            holder.title.setTextColor(ContextCompat.getColor(holder.itemView.context,android.R.color.holo_red_dark))
        }else{
            holder.title.setTextColor(ContextCompat.getColor(holder.itemView.context,R.color.day_black))
        }

        holder.event.setOnClickListener {
            listener.taskSetToCalendar(dataList[position])
        }


    }

    inner class ViewHolder(view : View) : RecyclerView.ViewHolder(view), View.OnClickListener{
        val title = view.findViewById<TextView>(R.id.title)
        val time = view.findViewById<TextView>(R.id.time)
        val date = view.findViewById<TextView>(R.id.date)
        val priority = view.findViewById<TextView>(R.id.priority)
        val event = view.findViewById<ImageView>(R.id.event)

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            listener.taskEditClicked(dataList[layoutPosition].id)
        }


    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun updateData(newData: List<TaskTable>){

        val oldList = this.dataList
        dataList = newData

        val diffUtil = DiffUtil.calculateDiff(object : DiffUtil.Callback(){
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = oldList[oldItemPosition].id == dataList[newItemPosition].id

            override fun getOldListSize(): Int = oldList.size

            override fun getNewListSize(): Int = dataList.size

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = oldList[oldItemPosition] == dataList[newItemPosition]

        })

        diffUtil.dispatchUpdatesTo(this)

    }


    interface Listener{
        fun deleteClicked(id : String)
        fun taskEditClicked(id: String)
        fun taskSetToCalendar(taskTable: TaskTable)
    }

    fun filterSearch(searchText : String? , dataList : List<TaskTable>) {
        if(searchText.isNullOrEmpty()){
            this.dataList = dataList
            notifyDataSetChanged()
        }else {
            val newList = mutableListOf<TaskTable>()
            dataList.forEach {
                if (it.title.contains(searchText, true)) {
                    newList.add(it)
                }
            }
            this.dataList = newList
            notifyDataSetChanged()
        }
    }

    override fun onItemSwipped(index: Int) {
        listener.deleteClicked(dataList[index].id)
    }

}