package com.example.task.View

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.widget.Toolbar
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.task.Database.TaskTable
import com.example.task.R
import java.util.*

class TaskCreateFragment :  BaseTaskFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val menuToolbar = view.findViewById<Toolbar>(R.id.menuToolbar)
        NavigationUI.setupWithNavController(menuToolbar, findNavController())
        menuToolbar.setNavigationIcon(R.drawable.back_arrow)
        menuToolbar.inflateMenu(R.menu.create_menu)
        menuToolbar.setNavigationOnClickListener {
            viewModel.dismissCreateTaskFragment()
        }

        super.onViewCreated(view, savedInstanceState)

        val title= view.findViewById<EditText>(R.id.title)
        val time= view.findViewById<EditText>(R.id.time)
        val date= view.findViewById<EditText>(R.id.date)
        val spinner= view.findViewById<Spinner>(R.id.spinner)


        view.post {
            menuToolbar?.menu?.findItem(R.id.create_ok)?.isVisible = title.text.isNotEmpty()
            menuToolbar?.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.create_ok -> {
                        viewModel.addTaskData(TaskTable(UUID.randomUUID().toString(),title.text.toString(),date.text.toString(),time.text.toString(),spinner.selectedItem.toString()))
                        true
                    }

                    else -> false
                }
            }
        }
    }

    override fun handleView(visible: Boolean) {
        requireView().post {
            val menuToolbar = requireView().findViewById<Toolbar>(R.id.menuToolbar)
            menuToolbar.menu.findItem(R.id.create_ok).isVisible = visible
        }
    }

}