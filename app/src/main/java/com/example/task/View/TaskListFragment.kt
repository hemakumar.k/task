package com.example.task.View

import TaskHelper
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.CalendarContract
import android.provider.CalendarContract.Events
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.task.Database.TaskTable
import com.example.task.R
import com.example.task.ViewModel.AppViewModel
import com.example.task.model.ViewModelUtil
import java.util.*


class TaskListFragment : Fragment(R.layout.task_list_layout) , TaskAdapter.Listener , androidx.appcompat.widget.SearchView.OnQueryTextListener {

    lateinit var viewModel : AppViewModel

    var dataList = listOf<TaskTable>()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        viewModel = ViewModelUtil.getAppViewModel(requireActivity() as AppCompatActivity)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val toolbar = requireActivity().findViewById<Toolbar>(R.id.toolbar)

        val search = toolbar.menu.findItem(R.id.search).actionView as androidx.appcompat.widget.SearchView

        search.setOnQueryTextListener(this)

        val recyclerView = requireView().findViewById<RecyclerView>(R.id.task_list)

        recyclerView.apply {
            adapter = TaskAdapter(listOf(),this@TaskListFragment)
        }


    }


    override fun onStart() {
        super.onStart()

        val recyclerView = requireView().findViewById<RecyclerView>(R.id.task_list)
        val emptyList = requireView().findViewById<TextView>(R.id.empty_list)
        val toolbar = requireActivity().findViewById<Toolbar>(R.id.toolbar)

        viewModel.listenTodoLiveData().observe(viewLifecycleOwner , Observer {

            dataList = it

            if(recyclerView.adapter == null){
                recyclerView.apply {
                    adapter = TaskAdapter(it,this@TaskListFragment)
                }
            }else{
                (recyclerView.adapter as TaskAdapter).updateData(it)
            }

            if(it.isEmpty()){
                recyclerView.visibility = View.GONE
                emptyList.visibility = View.VISIBLE
                toolbar.menu.findItem(R.id.search).isVisible = false

            }else{
                recyclerView.visibility = View.VISIBLE
                emptyList.visibility = View.GONE
                toolbar.menu.findItem(R.id.search).isVisible = true
            }

        })

        val callback = TaskHelper(adapter = recyclerView.adapter as TaskAdapter)
        ItemTouchHelper(callback).attachToRecyclerView(recyclerView)

    }

    override fun onStop() {
        super.onStop()
        viewModel.listenTodoLiveData().removeObservers(viewLifecycleOwner)
    }

    override fun deleteClicked(id: String) {
        viewModel.deleteTaskData(id)
    }

    override fun taskEditClicked(id: String) {
        if(findNavController().currentDestination!!.id == R.id.TaskListFragment){
            val bundle = Bundle()
            bundle.putString("taskID",id)
            findNavController().navigate(R.id.action_TaskListFragment_to_TaskEditFragment,bundle)
        }
    }

    override fun taskSetToCalendar(taskTable: TaskTable) {

        val intent = Intent(Intent.ACTION_INSERT)
        intent.type = "vnd.android.cursor.item/event"
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR,1)
        val startTime = calendar.timeInMillis
        val endTime = calendar.timeInMillis + 60 * 60 * 1000
        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startTime)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            intent.putExtra(CalendarContract.EXTRA_EVENT_ID, taskTable.id)
        }
        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime)
        intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false)
        intent.putExtra(Events.TITLE, taskTable.title)
        startActivity(intent)

    }


    override fun onQueryTextSubmit(p0: String): Boolean {
        return true
    }

    override fun onQueryTextChange(p0: String?): Boolean {
        if(findNavController().currentDestination!!.id == R.id.TaskListFragment) {
            val recyclerView = requireView().findViewById<RecyclerView>(R.id.task_list)
            if (recyclerView.adapter != null) {
                (recyclerView.adapter as TaskAdapter).filterSearch(p0, dataList)
            }
        }
        return false
    }
}