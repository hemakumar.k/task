package com.example.task.View

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.task.R
import com.example.task.ViewModel.AppViewModel
import com.example.task.model.ViewModelUtil
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import java.util.*


abstract class BaseTaskFragment : Fragment(R.layout.create_todo_layout) , DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    lateinit var viewModel : AppViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        viewModel = ViewModelUtil.getAppViewModel(requireActivity() as AppCompatActivity)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val title= view.findViewById<EditText>(R.id.title)
        val time= view.findViewById<EditText>(R.id.time)
        val date= view.findViewById<EditText>(R.id.date)
        val clock= view.findViewById<ImageView>(R.id.time_clock)
        val calendar_view= view.findViewById<ImageView>(R.id.calendar)
        val spinner= view.findViewById<Spinner>(R.id.spinner)


        calendar_view.setOnClickListener {
            val calendar = Calendar.getInstance()
            val datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
            if(requireActivity().supportFragmentManager.findFragmentByTag("datePickerDialog") == null) {
                datePickerDialog.show(requireActivity().supportFragmentManager, "datePickerDialog")
            }
        }

        clock.setOnClickListener {
            val calendar = Calendar.getInstance()
            val timePickerDialog = TimePickerDialog.newInstance(this, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false)
            if(requireActivity().supportFragmentManager.findFragmentByTag("timePickerDialog") == null) {
                timePickerDialog.show(requireActivity().supportFragmentManager, "timePickerDialog")
            }
        }

        view.findViewById<ImageView>(R.id.clear_date).setOnClickListener{
            date.text.clear()
        }

        view.findViewById<ImageView>(R.id.clear_time).setOnClickListener{
            time.text.clear()
        }

        val arrayAdapter = ArrayAdapter<String>(view.context, android.R.layout.simple_spinner_item, arrayListOf("LOW","MEDIUM" , "HIGH"))
        arrayAdapter.setDropDownViewResource(R.layout.spinner_layout)
        spinner.adapter = arrayAdapter

        title.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                handleView(title.text.isNotBlank())
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })


    }

    abstract fun handleView(visible: Boolean)


    override fun onDateSet(view: DatePickerDialog, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        val date= requireView().findViewById<EditText>(R.id.date)
        date.setText("$dayOfMonth-${monthOfYear+1}-$year")
    }

    override fun onTimeSet(view: TimePickerDialog, hourOfDay: Int, minute: Int, second: Int) {
        val time= requireView().findViewById<EditText>(R.id.time)
        var hour = hourOfDay
        var slot = "AM"
        if(hourOfDay / 12 != 0 ){
            hour = hourOfDay - 12
            slot = "PM"
        }
        time.setText("$hour : $minute $slot")
    }


    override fun onStop() {
        super.onStop()
        val viewShadow = view
        if (viewShadow != null) {
            val inputMethodManager = viewShadow.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(viewShadow.windowToken, 0)
        }
    }

}