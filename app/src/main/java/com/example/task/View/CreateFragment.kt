package com.example.task.View

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.*
import android.widget.FrameLayout
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.example.task.R
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class CreateFragment : BottomSheetDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.create_layout, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return EditorBottomMenuDialog(this.requireContext(), this.theme, object : MenuDialogCallback {
            override fun onBackPressed(): Boolean {
                val navController = view!!.findViewById<View>(R.id.navHostPlaceHolder).findNavController()
                if (navController.graph.startDestination == navController.currentDestination!!.id) {
                    return false
                }
                navController.navigateUp()
                return true
            }
        })
    }

    inner class EditorBottomMenuDialog(context: Context, themeID: Int, private val menuDialogCallback: MenuDialogCallback) : BottomSheetDialog(context, themeID) {

        override fun onBackPressed() {
            if (!menuDialogCallback.onBackPressed()) {
                super.onBackPressed()
            }
        }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            val bottomSheet = dialog?.findViewById(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
            val layoutParams = bottomSheet.layoutParams
            if (layoutParams != null) {
                layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
                layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
            }
            window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            bottomSheet.layoutParams = layoutParams
            window?.setGravity(Gravity.END)
            dialog?.setCanceledOnTouchOutside(false)
        }
    }

    override fun onStart() {
        super.onStart()

        // For Removing background blur view
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)


        // For disabling the bottom sheet from dragging
        val bottomSheet = dialog?.findViewById(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
        val mBehavior = BottomSheetBehavior.from(bottomSheet)

        // For setting to full screen view
        mBehavior.state = BottomSheetBehavior.STATE_EXPANDED

        mBehavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    mBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                }
                if (newState == BottomSheetBehavior.STATE_HALF_EXPANDED) {
                    mBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                }
            }
        })

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState == null) {

            val navHostFragment: NavHostFragment = NavHostFragment.create(R.navigation.nav_graph_create, arguments)

            childFragmentManager.beginTransaction()
                .replace(R.id.navHostPlaceHolder, navHostFragment)
                .setReorderingAllowed(true)
                .setPrimaryNavigationFragment(navHostFragment) //Equivalent to defaultNavHost="true" in xml
                .commitNow()
        }
    }

    override fun onCancel(dialog: DialogInterface) {
        dismiss()
    }
}

interface MenuDialogCallback {
    fun onBackPressed(): Boolean
}