package com.example.task.Database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.TypeConverters
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "TodoTable", primaryKeys = ["id"])
@TypeConverters(TaskTypeConverter::class)
data class TaskTable (

    @ColumnInfo(name = "id")
    @SerializedName("TODO_ID")
    @Expose
    var id: String = "",

    @ColumnInfo(name = "title")
    @SerializedName("TITLE")
    @Expose
    var title: String = "",

    @ColumnInfo(name = "date")
    @SerializedName("DATE")
    @Expose
    var date: String = "",

    @ColumnInfo(name = "time")
    @SerializedName("TIME")
    @Expose
    var time: String = "",

    @ColumnInfo(name = "priority")
    @SerializedName("PRIORITY")
    @Expose
    var priority: String = ""
)