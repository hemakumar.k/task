package com.example.task.Database

import androidx.room.TypeConverter
import java.util.*

class TaskTypeConverter {

    @TypeConverter
    fun dateToString(date: Date?): String? {
        if (date == null) return null else return date.time.toString()
    }

    @TypeConverter
    fun stringToDate(string: String?): Date? {
        if (string == null) return null else return Date(string.toLong())
    }
}