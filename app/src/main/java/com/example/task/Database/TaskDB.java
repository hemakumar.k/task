package com.example.task.Database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {TaskTable.class}, version = 1)
public abstract class TaskDB extends RoomDatabase {
    public abstract TaskDBDao todoDBDao();
}
