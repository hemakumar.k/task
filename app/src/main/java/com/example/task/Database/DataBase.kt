package com.example.task.Database

import android.content.Context
import androidx.room.Room
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

object DataBase {

    var taskDatabase: TaskDB? = null
        get() {
            if (field == null) {
                throw RuntimeException("Database has not been initialized ")
            }
            return field
        }


    fun initializeDB(context: Context) {
        GlobalScope.launch {
           taskDatabase = Room.databaseBuilder(context, TaskDB::class.java, "TodoApp").allowMainThreadQueries().build()

        }.start()
    }
}