package com.example.task.Database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query

@Dao
interface TaskDBDao {

    @Insert(onConflict = REPLACE)
    fun addTodo(task : TaskTable)

    @Query("UPDATE TodoTable SET title =:title ,date =:date , time = :time , priority = :priority  WHERE id = :id")
    fun updateTable(id: String, title : String, date : String ,priority : String , time : String)

    @Query("DELETE FROM TodoTable WHERE id = :id ")
    fun deleteFromTable(id: String)

    @Query( " SELECT * FROM TodoTable")
    fun getAllTodoList() : List<TaskTable>


    @Query( " SELECT * FROM TodoTable WHERE id = :id")
    fun getTodoData(id: String) : TaskTable

    @Query( " SELECT * FROM TodoTable")
    fun getTodoDataList() : LiveData<List<TaskTable>>


}