package com.example.task.ViewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.task.Database.TaskTable
import com.example.task.model.CreateTask
import com.example.task.model.TaskViewState
import com.example.task.network.RepositoryImpl
import kotlinx.coroutines.launch

class AppViewModel(application: Application ,var repository: RepositoryImpl) : AndroidViewModel(application) {

    private val todoTableList = repository.getAllTodoList()

    private val todoTableLiveData = repository.listenAllData()

    private val taskViewState = MutableLiveData<TaskViewState>()

    fun listenTodoLiveData() = todoTableLiveData

    fun taskViewState() = taskViewState as LiveData<TaskViewState>

    fun addTaskData(taskTable: TaskTable){
        viewModelScope.launch {
            repository.addTodo(taskTable)
            taskViewState.postValue(CreateTask)
        }
    }

    fun updateTaskData(id: String, title: String, date: String, priority : String, time: String){
        viewModelScope.launch {
            repository.updateTodo(id, title, date, priority, time)
        }
    }

    fun deleteTaskData(id: String){
        viewModelScope.launch {
            repository.deleteTodo(id)
        }
    }

    fun getTaskData(id: String) : TaskTable {
        return repository.getTodoData(id)
    }

    fun dismissCreateTaskFragment() {
        taskViewState.value = CreateTask
    }

}