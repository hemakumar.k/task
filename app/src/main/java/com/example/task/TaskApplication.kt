package com.example.task

import android.app.Application
import com.example.task.Database.DataBase

class TaskApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        DataBase.initializeDB(applicationContext)
        //AppComponentProvider.init()
    }
}