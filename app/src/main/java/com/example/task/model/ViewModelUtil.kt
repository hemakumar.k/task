package com.example.task.model

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.task.Database.DataBase
import com.example.task.ViewModel.AppViewModel
import com.example.task.network.RepositoryImpl

object ViewModelUtil {
    fun getAppViewModel(activity: AppCompatActivity): AppViewModel {
        return ViewModelProviders.of(activity, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                val db by lazy {
                    DataBase.taskDatabase
                }
                val repo = RepositoryImpl(db!!)
                @Suppress("UNCHECKED_CAST")
                return AppViewModel(activity.application ,repo) as T
            }
        })[AppViewModel::class.java]
    }
}