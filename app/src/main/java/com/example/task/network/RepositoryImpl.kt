package com.example.task.network

import androidx.lifecycle.LiveData
import com.example.task.Database.TaskDB
import com.example.task.Database.TaskTable

class RepositoryImpl(private val taskDB: TaskDB) : Repository {
    override fun addTodo(taskTable: TaskTable) {
        taskDB.todoDBDao().addTodo(taskTable)
    }

    override fun updateTodo(id: String, title: String, date: String, priority : String, time: String) {
        taskDB.todoDBDao().updateTable(id,title, date ,priority,time)
    }

    override fun getAllTodoList(): List<TaskTable> {
        return taskDB.todoDBDao().getAllTodoList()
    }

    override fun deleteTodo(id: String) {
        taskDB.todoDBDao().deleteFromTable(id)
    }

    override fun getTodoData(id: String) : TaskTable = taskDB.todoDBDao().getTodoData(id)

    override fun listenAllData(): LiveData<List<TaskTable>> {
        return taskDB.todoDBDao().getTodoDataList()
    }
}