package com.example.task.network

import androidx.lifecycle.LiveData
import com.example.task.Database.TaskTable

interface Repository {

    fun addTodo(taskTable: TaskTable)

    fun updateTodo(id : String , title : String , date: String , priority: String , time : String)

    fun getAllTodoList() : List<TaskTable>

    fun deleteTodo(id: String)

    fun getTodoData(id: String) : TaskTable

    fun listenAllData() : LiveData<List<TaskTable>>

}